classdef OutputClass < handle
    properties
        inaTrade
        tradeList
        data
        winRate
        trade
        
    end
    methods
        function obj = OutputClass()
            obj.tradeList = [];
            obj.inaTrade = false;
            obj.winRate = 0;
        end
        function updateData(obj,data)
            obj.data = data;
        end
        function y = inTrade(obj,i)
            if obj.inaTrade 
                obj.tradeList(end).calcHighE(obj.data.High(i));
                obj.tradeList(end).calcLowE(obj.data.Low(i));
            end
            y = obj.inaTrade;
        end
        function open(obj,date,entryPrice,units,trigOpen)
            obj.inaTrade = true;
            TD = TradeData(date,entryPrice,units,trigOpen);
            obj.trade = TD;
            obj.tradeList = [obj.tradeList;TD];
        end
        function close(obj,date,exitPrice,trigClose)
            obj.inaTrade = false;
            obj.tradeList(end).closeTrade(date,exitPrice,trigClose);
        end
        function performStat(obj)
            win = 0;
            for d = 1:length(obj.tradeList)
                if obj.tradeList(d).Winner == true
                win = win+1;
                end
            end
            obj.winRate = win/length(obj.tradeList);
        end

    end
end
