classdef TradeData < handle
   properties
      EntryDate
      ExitDate
      EntryPrice
      ExitPrice
      MFE = 0
      MAE = 0
      maxUnits = 0
      Units
      UnitPrice
      UnitProfit
      TrigOpen
      TrigClose
      Type
      Winner
   end
   methods
      function obj = TradeData(date,entryPrice,units,trigOpen)
         if nargin > 0
            obj.EntryDate = date;
            obj.EntryPrice = entryPrice;
            obj.Units = units;
            obj.UnitPrice = entryPrice;
            obj.UnitProfit = 0;
            obj.MFE = 0;
            obj.MAE = 0;
            obj.TrigOpen = trigOpen;
            obj.maxUnits = units;
            if units > 0
                obj.Type = true;
            else
                obj.Type = false;
            end
         end
      end
      function change(obj,price,units)
          if units + obj.Units == 0
              disp('Cannot subtract from position because units = 0, use close instead')
          else
              newPrice = price*units + obj.UnitPrice*obj.Units;
              newUnits = units + obj.Units;
              newPrice = newPrice/newUnits;
              if obj.Units < 0
                  obj.unitProfit = (obj.UnitPrice - newPrice)*(-units);
              else
                  obj.unitPrice = newPrice;
              end
              obj.Units = newUnits;
              obj.maxUnits = max(obj.Units,obj.maxUnits);
          end
      end
      function closeTrade(obj,date,exitPrice,trigClose)
          obj.ExitDate = date;
          obj.ExitPrice = exitPrice;
          obj.TrigClose = trigClose;
          obj.calcHighE(exitPrice);
          obj.calcLowE(exitPrice);
          if obj.Units < 0
              t = obj.MFE;
              obj.MFE = obj.MAE;
              obj.MAE = t;
              if obj.EntryPrice < obj.ExitPrice
                  obj.Winner = true;
              else
                  obj.Winner = false;
              end
          else
              if obj.EntryPrice < obj.ExitPrice
                  obj.Winner = true;
              else
                  obj.Winner = false;
              end
          end
          
          % TODO unitprofit, totalprofit, cost
      end
      function calcHighE(obj,high)
          obj.MFE = max((high-obj.UnitPrice)*obj.Units,obj.MFE); 
      end
      function calcLowE(obj,low)
          obj.MAE = min((low-obj.UnitPrice)*obj.Units,obj.MAE); 
      end
          
   end
end