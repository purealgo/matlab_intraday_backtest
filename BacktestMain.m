clear;
load ES.mat

%% Input Data

%select start and end date from data
tStart = datetime(2015,1,1);
tEnd = datetime(2017,2,1);
ESidx = isbetween(ES.Date,tStart,tEnd);
ES = ES(ESidx,:);

%USD
tickvalue = 12.50;
ticksize = .25;
cost = 1.98;

startMin = 5;
endMin = 31;
closePos = 404;

frontRun = 1;
profitTarget = 1000;
stopLoss = 1000;

%% Backtest

% ... Can loop over, Groupings go from 1->numDays
output = OutputClass;

dateGroups = dateshift(ES.Date, 'start', 'day');
G = findgroups(dateGroups);
days = unique(dateGroups);

tic
for d = 1:numel(days)
   data = ES(G==d, :);
   
    %Buy Indicator, Sell Indicator, profit target, stop loss
    v1 = frontRun*ticksize; v2 = 0; pt = profitTarget*ticksize; st = stopLoss*ticksize;
   fbacktest(data,v1,v2,pt,st,startMin,endMin,closePos,output);
   
end
toc

output.performStat

length = length(output.tradeList);
t = [output.tradeList];

warning('off','all')
for i = 1 : length
    s(i) = struct(t(i));
end

table = struct2table(s);

table.PnL = table.ExitPrice - table.EntryPrice;

NumTrades = numel(table.PnL)
Returns = sum(table.PnL)
Expectancy = Returns/numel(table.PnL)
WinRate = output.winRate*100
[MaxDrawDown,MaxDrawDownDuration] = calculateMaxDD(cumsum(table.PnL))
%% Write to Excel

filename = 'BacktestResults.xlsx';
A = {'# of Trades','Expectancy','Win Rate','Returns','Max Drawdown','Max Drawdown Duration'; ...
    NumTrades,Expectancy,WinRate,Returns,MaxDrawDown,MaxDrawDownDuration};
sheet = 1;
xlRange = 'B1';
%xlswrite(filename,A,sheet,xlRange)
writetable(table,filename);

%% Plot Results

Trade_Plots(ES.High,ES.Low,ES.Close,ES.Open,ES.Date,height(table),table.EntryDate,table.ExitDate,table.EntryPrice,table.ExitPrice)
% candlestick(ES.High,ES.Low,ES.Close,ES.Open,'k',ES.Date)