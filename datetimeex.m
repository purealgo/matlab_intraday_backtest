clear;
load ES.mat

dateGroups = dateshift(ES.Date, 'start', 'day');
G = findgroups(dateGroups);
%numDays = unique(dateGroups);

numDays = max(G);
x = cell(1,numDays);

for i = 1:numDays
   day = ES(G==i, :);
   six41 = modifyDateTime(day.Date, 6, 41);
   twelve35 = modifyDateTime(day.Date, 12, 35);
   
   timeFrame = day(day.Date >= six41 & day.Date <= twelve35, :);
   x{i} = timeFrame; % Braces make this a cell array.
end

function newDatetime = modifyDateTime(dt, newHour, newMin)
    newDatetime = dt;
    newDatetime.Hour = newHour;
    newDatetime.Minute = newMin;
end