% Forcast and Backtest
 ES = readtable('ESH7_ALL.txt');
 
 %% Removes entire rows of bad or missing data
TF = ismissing(ES);
ES = ES(~any(TF,2),:);

%% Converts Date and Time into 'datetime'

 datetimesVec = datetime(ES.Date);
 ES.Date = datetimesVec;
 
%% Extracts intraday Data   
% {'11-Feb-2009 06:00:00'}
ESintraidx = timeofday(ES.DateTime) >= timeofday(datetime('06:01:00')) & timeofday(ES.DateTime) <= timeofday(datetime('11:29:00'));
SPY = ES(ESintraidx,:);