function longmidtrade(data,v1,pt,st,startMin,endMin,closePos,output)

ntrade = 0;
high = data.High; low = data.Low; indBuy = [0;data.PITMID(2:end)]; close = data.Close;
output.updateData(data); PrevHighbroken = 0; PrevLowbroken = 0; MidStrat = 0;

for i=1:height(data)
  
     if i < 30
        if low(i) < data.PrevLow(i)
            PrevLowbroken = 1;
        end
        if high(i) > data.PrevHigh(i)
            PrevHighbroken = 1;
        end
     end
%         if high(i) > max(high(1:30))
%             MidStrat = 1;
%         elseif low(i) < data.PITMID(i-1)
%             MidStrat = 0;
%         end

    
    if i >= startMin && i <= endMin && output.inTrade(i) == false
            %%&& (data.Open(1) < data.PrevHigh(i) || data.Open(1) > data.PrevLow(i))       

        if (low(i)-indBuy(i-1))<v1 && (low(i-1)-indBuy(i-2))>=v1 ...
                && ntrade ==0 && PrevHighbroken == 1 && PrevLowbroken == 0 && height(data) == 405
            
            ntrade = ntrade + 1;
            output.open(data.Date(i),indBuy(i-1)+v1,1,'Long');
            
        end
        
    end
            
            %If you are long and hit a profit target
            if output.inTrade(i) == true && high(i)>output.trade.EntryPrice+pt
                
                output.close(data.Date(i),(output.trade.EntryPrice+pt),'Target');               
                
     
                %If you are long and hit a stop loss, run this code
            elseif output.inTrade(i) == true && low(i)<=output.trade.EntryPrice-st
                
                output.close(data.Date(i),(output.trade.EntryPrice-st),'Stop');               
                
                
            elseif output.inTrade(i) == true && i > closePos

                output.close(data.Date(i),close(i),'EOD');               
               
            end
            
end
     

end
