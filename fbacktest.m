function fbacktest(data,v1,v2,pt,st,startMin,endMin,closePos,output)

iter=1; buy(iter)=0; sell(iter)=0; ntrade = 0;
high = data.High; low = data.Low; indBuy = [0;data.ONL(2:end)]; indSell = [0;data.HighestH(2:end)]; close = data.Close;
output.updateData(data);

for i=1:height(data)
    
    if i >= startMin && i <= endMin && output.inTrade(i) == false && low(1) > data.PrevLow(i)

        if (low(i)-indBuy(i-1))<-v1 && (low(i-1)-indBuy(i-2))>=-v1 && buy(iter)==0 && sell(iter)==0 ...
                && ntrade ==0
            
            buy(iter)=1;
            buyPrice(iter)=indBuy(i-1)-v1;
            ntrade = ntrade + 1;
            output.open(data.Date(i),indBuy(i-1)-v1,1,'Long');
            
        end
        
        
        %if short indicator gets turned on sell
        if (high(i)-indSell(i-1))>v2 && (high(i-1)-indSell(i-2))<v2 && sell(iter)==0 && buy(iter)==0 ...
                && ntrade ==0
            
            sell(iter)=1;
            sellPrice(iter)=indSell(i-1)+v2;
            ntrade = ntrade + 1;
            output.open(data.Date(i),indSell(i-1)+v2,-1,'Short');
        end
    end
            
            %If you are long and hit a profit target
            if buy(iter)==1 && high(i)>buyPrice(iter)+pt
                
                buy(iter)=0;
                sell(iter)=0;
                output.close(data.Date(i),(indBuy(i-1)-v1)+pt,'Target');               
                
     
                %If you are long and hit a stop loss, run this code
            elseif buy(iter)==1 && low(i)<=buyPrice(iter)-st
                
                iter=iter+1;
                buy(iter)=0;
                sell(iter)=0;
                output.close(data.Date(i),(indBuy(i-1)-v1)-st,'Stop');               
                
                
            elseif buy(iter)==1 && i > closePos

                iter=iter+1;
                buy(iter)=0;
                sell(iter)=0;
                output.close(data.Date(i),close(i),'EOD');               
               
            end
            

            %If you are short, and hit a profit target, run this code
            if sell(iter)==1 && low(i)<sellPrice(iter)-pt
                
                iter=iter+1;
                buy(iter)=0;
                sell(iter)=0;
                
                
                %If you are short, and hit a stop loss, run this code
            elseif sell(iter)==1 && high(i)>=sellPrice(iter)+st
                
                iter=iter+1;
                buy(iter)=0;
                sell(iter)=0;
                
                
            elseif sell(iter)==1 && i > closePos

                iter=iter+1;
                buy(iter)=0;
                sell(iter)=0;
                
            end
end
     

end
