function Trade_Plots(H,L,C,O,D,n,EntryDate,ExitDate,EntryPrice,ExitPrice)

    candlestick(H,L,C,O,'k',D)
    set(gca,'LooseInset',get(gca,'TightInset'))
    datetick('x','keepticks','keeplimits')
%     tick_index = [1:round(length(D)/5):length(D) length(D)]; 
%     tick_index = unique(tick_index); % checks length of the dates with 10 steps in between.
%     tick_label  = datestr(D(tick_index)); % this is translated to a datestring.
%     set(gca,'XTick',tick_index)
%     set(gca,'XTickLabel',tick_label);
    
        if n == 10
            datetick('x',10)
        else
            datetick('x',3)
        end

    hold on
    plot(datenum(EntryDate),EntryPrice,...
    'g^','Linewidth',1.0)
    hold on
    plot(datenum(ExitDate),ExitPrice,...
    'r^','Linewidth',1.0)
    grid on
    
end

  