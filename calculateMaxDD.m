function [maxDD,maxDDD]=calculateMaxDD(returns)

% calculation of maximum drawdown and maximum drawdown duration
[peak,ipeak] = max(returns);
[valley,ivalley] = min(returns);
high = max(returns(1:ivalley));
low = min(returns(ipeak:end));

if (high-valley) > (peak-low)
        maxDD = high-valley;
    else
        maxDD = peak-low;
end

highwatermark=zeros(size(returns)); % initialize high watermarks to zero.

drawdown=zeros(size(returns)); % initialize drawdowns to zero.

drawdownduration=zeros(size(returns)); % initialize drawdown duration to zero.

for t=2:length(returns)
    highwatermark(t)=max(highwatermark(t-1), returns(t));
    drawdown(t)=(1+returns(t))./(1+highwatermark(t))-1; % drawdown on each day
    if (drawdown(t)==0)
        drawdownduration(t)=0;
    else
        drawdownduration(t)=drawdownduration(t-1)+1;
    end
end


maxDDD=max(drawdownduration); % maximum drawdown duration
